package ssw;

public class MedicalInsurance extends InsurancePolicy {
    private String personName;
    private int degree;
    private int age;

    public MedicalInsurance(int policyNumber, String personName, int degree, int age) {
        super(policyNumber);
        this.personName = personName;
        this.age = age;
        setDegree(degree);
    }

    @Override
    String getName() {
        return "ssw.MedicalInsurance";
    }

    @Override
    double calculatePrice() {
        double cost = 0;
        switch (degree) {
            case 1:
                cost = 200;
                break;
            case 2:
                cost = 250;
                break;
            case 3:
                cost = 300;
                break;
        }

        if (age > 40)
            cost *= 1.2;

        return cost;
    }

    public String getPersonName() {
        return personName;
    }

    public int getDegree() {
        return degree;
    }

    public int getAge() {
        return age;
    }

    public MedicalInsurance setPersonName(String personName) {
        this.personName = personName;
        return this;
    }

    public MedicalInsurance setDegree(int degree) {
        if (degree > 0 && degree < 4)
            this.degree = degree;
        return this;
    }

    public MedicalInsurance setAge(int age) {
        this.age = age;
        return this;
    }
}
