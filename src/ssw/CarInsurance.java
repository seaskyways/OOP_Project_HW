package ssw;

public class CarInsurance extends InsurancePolicy {
    private int kind;
    private String carNumber;
    private int carYear;

    public CarInsurance(int policyNumber, int kind, String carNumber, int carYear) {
        super(policyNumber);
        this.kind = kind;
        this.carNumber = carNumber;
        this.carYear = carYear;
    }

    @Override
    String getName() {
        return "ssw.CarInsurance";
    }

    @Override
    double calculatePrice() {
        double cost = 0;
        switch (kind) {
            case 1:
                cost = 250;
                break;
            case 2:
                cost = 300;
                break;
        }

        if (carYear > 2000)
            cost *= 1.15;

        return cost;
    }
}
