package ssw;

public abstract class InsurancePolicy {
    private int policyNumber;
    private static double accPrice;

    public InsurancePolicy(int policyNumber) {
        this.policyNumber = policyNumber;
        accPrice += calculatePrice();
    }

    abstract String getName();

    abstract double calculatePrice();

    public int getPolicyNumber() {
        return policyNumber;
    }

    public static double getAccPrice() {
        return accPrice;
    }
}
