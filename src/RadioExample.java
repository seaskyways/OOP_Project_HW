import javax.swing.*;
import java.awt.*;

public class RadioExample {
    public static void main(String[] args) {
        JFrame jFrame = new JFrame();
        jFrame.setLayout(new FlowLayout());


        JRadioButton r1 = new JRadioButton("Left");
        JRadioButton r2 = new JRadioButton("Right");
        JRadioButton[] options = new JRadioButton[]{r1, r2,};

        jFrame.add(r1);
        jFrame.add(r2);

        ButtonGroup radio = new ButtonGroup();
        radio.add(r1);
        radio.add(r2);

        JLabel label = new JLabel();
        jFrame.add(label);

        JButton button = new JButton("Tell me who's selected");
        jFrame.add(button);
        button.addActionListener(e -> {
            if (radio.getSelection() == null) return; // nothing is selected

            for (JRadioButton option : options) {
                if (option.isSelected()) {
                    label.setText("Selected is :" + option.getText());
                    break;
                }
            }
        });


        jFrame.setSize(400, 400);
        jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        jFrame.setVisible(true);
    }
}
