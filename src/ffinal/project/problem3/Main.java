package ffinal.project.problem3;

import javax.swing.*;

public class Main {

    static Invoice[] invoices;

    public static void main(String[] args) {
        {
            int N = Integer.parseInt(JOptionPane.showInputDialog("How many invoices ?"));
            invoices = new Invoice[N];
        }


    }

    static String getTotalPriceMsg() {
        return "Total price :" + Invoice.getTotalInvoicesSum();
    }

    static String getLocalSuppliersSpecialMsg() {
        StringBuilder str = new StringBuilder();
        for (Invoice inv : invoices) {
            if (inv instanceof SupplierInvoice) {
                final SupplierInvoice supplierInvoice = (SupplierInvoice) inv;
                final Person supplier = supplierInvoice.getSupplier();
                if (supplierInvoice.invoiceCharge() > 5000 && supplierInvoice.isLocal()) {
                    str.append(supplier.getFirstName())
                            .append(" ")
                            .append(supplier.getLastName())
                            .append("\n");
                }
            }
        }

        return str.toString();
    }

}
