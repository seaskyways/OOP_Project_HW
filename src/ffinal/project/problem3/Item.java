package ffinal.project.problem3;

public class Item {
    private int code;
    private String brand;
    private String name;
    private double unitCost;
    private int quantity;

    public Item(int code, String brand, String name, double unitCost, int quantity) {
        this.code = code;
        this.brand = brand;
        this.name = name;
        this.unitCost = unitCost;
        this.quantity = quantity;
    }

    public int getCode() {
        return code;
    }

    public Item setCode(int code) {
        this.code = code;
        return this;
    }

    public String getBrand() {
        return brand;
    }

    public Item setBrand(String brand) {
        this.brand = brand;
        return this;
    }

    public String getName() {
        return name;
    }

    public Item setName(String name) {
        this.name = name;
        return this;
    }

    public double getUnitCost() {
        return unitCost;
    }

    public Item setUnitCost(double unitCost) {
        this.unitCost = unitCost;
        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public Item setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    @Override
    public String toString() {
        return "Item{" +
                "code=" + code +
                ", brand='" + brand + '\'' +
                ", name='" + name + '\'' +
                ", unitCost=" + unitCost +
                ", quantity=" + quantity +
                '}';
    }
}
