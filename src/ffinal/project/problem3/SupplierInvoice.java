package ffinal.project.problem3;

public class SupplierInvoice extends Invoice {
    private Person supplier;
    private Item suppliedItem;
    private double balance;
    private boolean local;

    public SupplierInvoice(int num, Person supplier, Item suppliedItem, double balance, boolean local) {
        super(num);
        this.supplier = supplier;
        this.suppliedItem = suppliedItem;
        this.balance = balance;
        this.local = local;
        init();
    }

    public SupplierInvoice() {
        super();
        this.supplier = new Person("", "", 0);
        this.suppliedItem = new Item(1, "", "", 0, 0);
        this.balance = 0;
        this.local = false;
        this.init();
    }

    @Override
    double invoiceCharge() {
        double price = suppliedItem.getQuantity() * suppliedItem.getUnitCost();
        if (local) {
            price *= 1.05;
        } else {
            price *= 1.1;
        }

        return price;
    }

    @Override
    String className() {
        return "SupplierInvoice";
    }

    @Override
    public String toString() {
        return "SupplierInvoice{" +
                "supplier=" + supplier +
                ", suppliedItem=" + suppliedItem +
                ", balance=" + balance +
                ", local=" + local +
                ", num=" + num +
                '}';
    }

    public Person getSupplier() {
        return supplier;
    }

    public SupplierInvoice setSupplier(Person supplier) {
        this.supplier = supplier;
        return this;
    }

    public Item getSuppliedItem() {
        return suppliedItem;
    }

    public SupplierInvoice setSuppliedItem(Item suppliedItem) {
        this.suppliedItem = suppliedItem;
        return this;
    }

    public double getBalance() {
        return balance;
    }

    public SupplierInvoice setBalance(double balance) {
        this.balance = balance;
        return this;
    }

    public boolean isLocal() {
        return local;
    }

    public SupplierInvoice setLocal(boolean local) {
        this.local = local;
        return this;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
