package ffinal.project.problem3;

public abstract class Invoice {
    protected int num;
    protected static int count;
    protected static double totalInvoicesSum;

    public Invoice(int num) {
        this.num = num;
    }

    public Invoice() {
        this(count);
    }

    void init() {
        count++;
        totalInvoicesSum += invoiceCharge();
    }

    abstract double invoiceCharge();

    abstract String className();

    @Override
    public String toString() {
        return "Invoice{" +
                "num=" + num +
                '}';
    }

    public int getNum() {
        return num;
    }

    public static int getCount() {
        return count;
    }

    public static double getTotalInvoicesSum() {
        return totalInvoicesSum;
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        totalInvoicesSum -= invoiceCharge();
        count -= 1;
    }
}
