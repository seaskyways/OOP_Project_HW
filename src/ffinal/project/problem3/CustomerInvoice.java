package ffinal.project.problem3;

import java.util.Arrays;

public class CustomerInvoice extends Invoice {
    int itemsCount;
    Item orderedItems[];
    Person customer;
    int type;

    public CustomerInvoice(int num, int itemsCount, Person customer, int type) {
        super(num);
        this.itemsCount = itemsCount;
        this.orderedItems = new Item[itemsCount];
        this.customer = customer;
        this.type = type;
        init();
    }

    public CustomerInvoice() {
        this.itemsCount = 0;
        this.orderedItems = new Item[itemsCount];
        this.customer = new Person("", "", 0);
        this.type = 1;
        init();
    }

    @Override
    double invoiceCharge() {
        double price = 0;

        for (Item item :
                orderedItems) {
            price += item.getQuantity() * item.getUnitCost();
        }
        if (type == 1) {
            price *= (1 - 0.07);
        }
        return price;
    }

    @Override
    String className() {
        return "CustomerInvoice";
    }

    @Override
    public String toString() {
        return "CustomerInvoice{" +
                "itemsCount=" + itemsCount +
                ", orderedItems=" + Arrays.toString(orderedItems) +
                ", customer=" + customer +
                ", type=" + type +
                '}';
    }

    public int getItemsCount() {
        return itemsCount;
    }

    public CustomerInvoice setItemsCount(int itemsCount) {
        this.itemsCount = itemsCount;
        return this;
    }

    public Item[] getOrderedItems() {
        return orderedItems;
    }

    public CustomerInvoice setOrderedItems(Item[] orderedItems) {
        this.orderedItems = orderedItems;
        return this;
    }

    public Person getCustomer() {
        return customer;
    }

    public CustomerInvoice setCustomer(Person customer) {
        this.customer = customer;
        return this;
    }

    public int getType() {
        return type;
    }

    public CustomerInvoice setType(int type) {
        if (type == 1 || type == 2)
            this.type = type;
        return this;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
