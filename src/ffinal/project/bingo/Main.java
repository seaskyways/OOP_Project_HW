package ffinal.project.bingo;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static int[][] randGrid = new int[5][5];
    static int[] userNumbers = new int[5];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        do {
            fillRandGrid();

//           for (int[] column : randGrid) {
//              System.out.println(Arrays.toString(column));
//           }

            fillUserGrid();
            checkWin();
            System.out.println("Do you want to play again ? (y/n)");
        } while (scanner.next().equals("y"));
    }

    static void fillRandGrid() {
        int[] column;
        for (int i = 0; i < randGrid.length; i++) {
            int offset = 15 * i;
            column = randGrid[i];

            for (int j = 0; j < column.length; j++) {
                int rand;
                boolean unique;
                do {
                    rand = ((int) Math.round((Math.random() * 15) + offset));
                    unique = true;
                    for (int k = j - 1; k >= 0; k--) {
                        if (column[k] == rand) {
                            unique = false;
                            break;
                        }
                    }
                } while (!unique);

                column[j] = rand;
            }
        }
    }

    static void fillUserGrid() {
        System.out.println("Enter your numbers now :");

        int input;
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < userNumbers.length; i++) {
            System.out.print("Number " + (i + 1) + " :");
            input = Integer.parseInt(scanner.next());
            userNumbers[i] = input;
        }
    }

    static boolean checkWin() {
        for (int[] aRandGrid : randGrid) {
            if (Arrays.equals(aRandGrid, userNumbers)) {
                System.out.println("We have a winner !");
                return true;
            }
        }

        int[] diag1 = new int[5];
        int[] diag2 = new int[5];
        for (int i = 0; i < randGrid.length; i++) {
            diag1[i] = randGrid[i][i];
            diag2[i] = randGrid[i][randGrid.length - i - 1];
        }
        if (Arrays.equals(diag1, userNumbers)) {
            System.out.println("We have a winner !");
            return true;
        }
        if (Arrays.equals(diag2, userNumbers)) {
            System.out.println("We have a winner !");
            return true;
        }

        return false;
    }
}
