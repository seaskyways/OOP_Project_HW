package ffinal.project.order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Orders extends JFrame implements ActionListener {

    private JButton btnCompute, btnClear, btnSave, btnShow;
    private JCheckBox cxkChess, cxkCheckers, cxkCrossword;
    private JRadioButton radDebit, radCredit, radECash;
    private JLabel lblResult;

    final ButtonGroup group = new ButtonGroup();

    public Orders() {
        setTitle("Orders");
        setLayout(new GridLayout(6, 2));

        JLabel tmp;
        tmp = new JLabel("products");
        add(tmp);

        tmp = new JLabel("payment method");
        add(tmp);

        cxkChess = new JCheckBox("Chess master (95$)");
        cxkCheckers = new JCheckBox("Checkers pro (40$)");
        cxkCrossword = new JCheckBox("Crossword maker (20$)");

        radDebit = new JRadioButton("Debit card");
        radCredit = new JRadioButton("Credit card");
        radECash = new JRadioButton("E-Cash");
        group.add(radDebit);
        group.add(radCredit);
        group.add(radECash);

        add(cxkChess);
        add(radDebit);
        add(cxkCheckers);
        add(radCredit);
        add(cxkCrossword);
        add(radECash);

        btnCompute = new JButton("Compute");
        btnCompute.addActionListener(this);

        btnClear = new JButton("Clear");
        btnClear.addActionListener(this);

        add(btnCompute);
        add(btnClear);

        tmp = new JLabel("you are paying :");
        add(tmp);

        lblResult = new JLabel("");
        add(lblResult);

        btnSave = new JButton("Save");
        btnShow = new JButton("Show");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnClear) {
            group.clearSelection();
            cxkCrossword.setSelected(false);
            cxkCheckers.setSelected(false);
            cxkChess.setSelected(false);
            lblResult.setText("");
        } else if (e.getSource() == btnCompute) {
            double price = 0;
            if (cxkChess.isSelected()) price += 95;
            if (cxkCheckers.isSelected()) price += 40;
            if (cxkCrossword.isSelected()) price += 20;

            if (radDebit.isSelected()) price *= 1.02;
            else if (radCredit.isSelected()) price *= 1.04;
            else if (radECash.isSelected()) price *= 1.06;

            lblResult.setText("You payed " + price);

            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter("orders", true));
                writer.append(lblResult.getText())
                        .append('\n');
                writer.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
