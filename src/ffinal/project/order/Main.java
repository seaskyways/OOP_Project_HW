package ffinal.project.order;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        Orders orders = new Orders();

        orders.setSize(400, 400);
        orders.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        orders.setVisible(true);
    }
}
