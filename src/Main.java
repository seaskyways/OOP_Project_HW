import ssw.CarInsurance;
import ssw.InsurancePolicy;
import ssw.MedicalInsurance;

import java.util.Scanner;

public class Main {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        InsurancePolicy[] insurancePolicies;
        {
            final int N = pi("Enter number of insurances: ");
            insurancePolicies = new InsurancePolicy[N];
        }

        for (int i = 0; i < insurancePolicies.length; i++) {
            String policyType;
            InsurancePolicy policy = null;
            do {
                pln("Please enter a valid policy type (C for Car, M for Medical)");
                policyType = scanner.nextLine();
            } while (policyType.length() == 0 || (policyType.charAt(0) != 'C' && policyType.charAt(0) != 'M'));

            int policyNumber = pi("enter Policy Number ");

            switch (policyType.charAt(0)) {
                case 'C':
                    int kind = Integer.parseInt(ps("enter Kind "));
                    String carNumber = ps("enter car Number ");
                    int carYear = pi("enter car year ");
                    policy = new CarInsurance(policyNumber, kind, carNumber, carYear);
                    break;

                case 'M':
                    String personName = ps("Enter person name ");
                    int degree = pi("Enter degree ");
                    int age = pi("Enter age ");
                    policy = new MedicalInsurance(policyNumber, personName, degree, age);
                    break;
            }

            insurancePolicies[i] = policy;
        }

        p(String.format(
                "Total price = %.2f", InsurancePolicy.getAccPrice()
        ));

        int carInsurancesCount = 0;
        for (InsurancePolicy policy : insurancePolicies) {
            if (policy instanceof CarInsurance)
                carInsurancesCount++;
        }
        p(String.format("Car insurances counts %d", carInsurancesCount));

        for (InsurancePolicy policy : insurancePolicies) {
            if (policy instanceof MedicalInsurance) {
                MedicalInsurance mi = (MedicalInsurance) policy;
                pln("MI { name = " + mi.getPersonName() + ", age = " + mi.getAge() + " } ");
            }
        }
    }

    static void p(Object o) {
        System.out.print(o);
    }

    static void pln(Object o) {
        System.out.println(o);
    }

    static String ps(Object out) {
        p(out);
        return scanner.nextLine();
    }

    static int pi(Object out) {
        try {
            p(out);
            return scanner.nextInt();
        } finally {
            scanner.nextLine();
        }
    }
}
